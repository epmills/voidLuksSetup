# voidLuksSetup

Bash script for installing Void Linux with disk encryption. Also performs some
post-install configuration, such as installing graphics drivers, a graphical DE,
and other applications, enabling/disabling services, creating a non-root user,
etc. The script is designed to be user-configurable, by modifying a number of
text fields near the start of the script prior to execution.

# References

Much of what is done in this script came almost straight from the official [Void
Documentation](https://docs.voidlinux.org/installation/guides/fde.html). I just
adapted commands where necessary to make them script-able, as well as made a
number of assumptions/personal choices as to what additional
configuration/utilities should be added.

I also used the [Arch Wiki](https://wiki.archlinux.org/) as a reference.

# Assumptions

A (non-exhaustive) list of the more fundatmental assumptions made by this
script:

1. Assumes an x86_64 system (basically any desktop/laptop within the last ~15
   years).
2. Assumes installation from a running Void live image
3. Assumes an EFI/GPT installation (I believe this should be compatible with the
   vast majority of systems that are less than ~10 years old)
4. Assumes that the entire installation (boot loader, root, home) is on the same
   drive
5. Assumes the installation will occupy the entire drive (the whole drive will
   be wiped pior to installation)
6. The installation will use a separate partition (volume) for swap (and
   optionally for home), in addition to the root partition (volume)

There are a number of other smaller assumptions made in various default values
populated in the script, but for the most part these are meant to be easily
changeable by the user by editing the fields located near the start of the
script file.

# Usage

1. Create a Void live image (instructions
   [here](https://docs.voidlinux.org/installation/live-images/prep.html)). You
   can use the base image or whichever graphical 'flavor' you'd like (it won't
   impact the new Void install you're creating).

2. Boot the live image, the login will be user:anon, password: voidlinux

3. For the Framework Laptop with Intel AX210 wireless, enable WIFI:

   ```
   $ sudo wpa_supplicant -i wlp170s0 \
       -c /etc/wpa_supplicant/wpa_supplicant.conf &
   $ wpa_cli -i wlp170s0
   > scan
   > scan_results
   > add_network
   > set_network 0 ssid "your-ssid"
   > set_network 0 psk "your-psk"
   > enable_network 0
   > quit
   $ ip link addr show
   $ ping -c 3 google.com
   ```

4. Execute:

   ```
   $ sudo xbps-install -Suy xbps
   ```

5. Download the script:

   ```
   $ sudo xbps-install -Suy git
   $ git clone https://gitlab.com/epmills/voidLuksSetup.git
   $ cd voidLuksSetup
   ```

6. Open `void_luks_setup.bash` in the editor.

7. Edit the fields in the first section based on your configuration, as per the
   comments in the script. Optionally, you can also edit the fields in the next
   two sections as well. Even if you don't configure the latter two sections
   prior to installation, it should generally be fairly easy to alter in the
   future by installing/removing packages and enabling/disabling services once
   you're up and running.

8. Execute:

   ```
   $ chmod +x void_luks_setup.bash
   $ sudo ./void_luks_setup.bash
   ```

9. When prompted, enter the desired passwords for LUKS encryption, root user,
   and non-root user

10. When prompted, select the desired drive for installation

11. Depending on what was previously on the installation drive, some warning(s)
    may be displayed about LUKS and/or filesystem signatures being already
    present on the drive, this is not an issue.

12. Wait for the installation to complete

13. Once the installation has completed, the user will be asked whether to
    automatically reboot.

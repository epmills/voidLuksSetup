#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace

if ! type cargo; then
  echo 'setting up rust'
  sudo xbps-install -Suy rustup && rustup-init
  echo 'initialize rust per instructions and rerun this script'
  exit 0
fi

sudo xbps-install base-devel libressl-devel

cargo install cargo-update

cargo install alacritty bat bottom exa starship
